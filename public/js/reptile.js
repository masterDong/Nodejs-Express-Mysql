const {  Button, Layout, Table, Space, Modal  } = antd;
const { Header, Content } = Layout;
class App extends React.Component {
    constructor(props) {
        super(props);    
        this.state = {
            collectList: [],
            pagination: {
                current: 1,
                pageSize: 10,
                total: 0
            },
            modalInfo: {
                title: "",
                visible: false
            }
        };  
    }

    componentDidMount() {
       this.getCollectList(this.state.pagination);
    }

    handleChangePage = (pagination, filters, sorter) => {
        this.getCollectList(pagination);
    }

    getCollectList(pagination) {
        let _this = this;
        let params = {
            current: pagination.current,
            pageSize: pagination.pageSize
        };
        let url = '/api/collect/getList'
        if (pagination) {
            let paramsArray = [];
            //拼接参数
            Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
            if (url.search(/\?/) === -1) {
                url += '?' + paramsArray.join('&')
            } else {
                url += '&' + paramsArray.join('&')
            }
        }
        fetch(url, {method: 'GET'})
        .then(response => response.json())
        .then((responseJSON) => {
            const { code, data, msg } = responseJSON;
            if (code == 1) {
                let pagination = this.state.pagination;
                pagination.total = pagination.total;
                _this.setState({
                    collectList: data.list,
                    pagination: pagination
                });
            } else {
                antd.message.error(msg);
            }
        })
        .catch((e)=> {
        });
    }

    handleModaCancel = (status) => {
        this.setState({
            visible: false
        });
    }

    handleModalOk = (status) => {
        this.setState({
            visible: true
        });
    }
  
    handleModalAdd = (status) => {
        this.setState({
            visible: true
        });
    }

    render() {
        const columns =  [
            {
                title: '名称',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: '规则',
                dataIndex: 'rule',
                key: 'rule',
                ellipsis: true,
            },
            {
                title: '地址',
                dataIndex: 'address',
                key: 'address',
                ellipsis: true,
            }
        ];
        const { collectList, pagination } = this.state;
        return (
            <Layout className="ant-layout-has-sider">
                <Sider />
                <Layout>    
                    <Header>Header</Header>
                    <Content className="padding-content">
                        <Space style={{ marginBottom: 16 }}>
                            <Button  onClick={ this.handleModalAdd }>新增采集规则</Button>
                        </Space>
                        <Table columns={ columns } rowKey={'id'} dataSource={ collectList } pagination={ pagination } onChange={ this.handleChangePage } />
                        <Modal
                            title={ this.state.modalInfo.title }
                            visible={ this.state.modalInfo.visible }
                            onOk={ this.handleModalOk }
                            onCancel={ this.handleModaCancel }
                        >
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                        </Modal>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

ReactDOM.render(
    <App />,
	document.getElementById('main')
);