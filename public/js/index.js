
class App extends React.Component {
    constructor(props) {
        super(props);    
        this.state = {
            date: 123
        };  
    }
  
    render() {
        const { date } = this.state;
        return (
            <antd.Layout>
                <Sider />
                <antd.Layout>    
                    <antd.Layout.Header>Header</antd.Layout.Header>
                    <antd.Layout.Content>{ date }</antd.Layout.Content>
                    <antd.Layout.Footer>Footer</antd.Layout.Footer>
                </antd.Layout>
            </antd.Layout>
        );
    }
}

ReactDOM.render(
    <App />,
	document.getElementById('main')
);