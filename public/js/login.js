class App extends React.Component {
    constructor(props) {
        super(props); 
        
        let loginLayoutBg = window.loginLayoutBg || '/images/body-bg.png';
        this.state = {
            layout: {
                labelCol: { span: 4 },
                wrapperCol: { span: 16 },
            },
            tailLayout: {
                wrapperCol: { offset: 4, span: 16 },
            },
            rules: {
                username: [{ required: true, message: '请输入账号!' }],
                password: [{ required: true, message: '请输入密码!' }]
            },
            loginLayoutBg: {
                background: `url(${loginLayoutBg}) center no-repeat fixed` 
            }
        };  
    }
    onFinish (params) {
        params.password = md5(params.password);
        fetch('/api/login', {
            headers: { 
                "Content-Type": "application/json"
            },
            method: 'POST',
            body:  JSON.stringify(params)
        })
        .then(response => response.json())
        .then((responseJSON) => {
            const { code, data, msg } = responseJSON;
            if (code == 1) {
                sessionStorage.setItem("userInfo", JSON.stringify(data));
                location.href = "/index";
            } else {
                antd.message.error(msg);
            }
        })
        .catch((e)=> {
        });
    };
  
    render() {
        const { layout, tailLayout, rules, loginLayoutBg } = this.state;
        return (
            <div className="login-layout" style={ loginLayoutBg}>
                <div className="login-form">
                    <antd.Form
                        {...layout}
                        name="basic"
                        initialValues={{ remember: true }}
                        onFinish={this.onFinish}
                        >
                        <antd.Form.Item
                            label="账号"
                            name="username"
                            rules={ rules.username }
                        >
                            <antd.Input />
                        </antd.Form.Item>

                        <antd.Form.Item
                            label="密码"
                            name="password"
                            rules={ rules.password }
                        >
                            <antd.Input.Password />
                        </antd.Form.Item>

                        <antd.Form.Item {...tailLayout} name="remember" valuePropName="checked">
                            <antd.Checkbox>记住密码</antd.Checkbox>
                        </antd.Form.Item>

                        <antd.Form.Item {...tailLayout}>
                            <antd.Button type="primary" htmlType="submit">登  录</antd.Button>
                        </antd.Form.Item>
                    </antd.Form>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
	document.getElementById('main')
);