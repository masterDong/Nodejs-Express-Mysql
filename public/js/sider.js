const { Layout, Button, Menu } = antd;
class Sider extends React.Component {
    constructor(props) {
        super(props);    
        this.state = {
            meunList: [
                {
                    name: '主页',
                    link: '/index',
                    icon: 'HomeOutlined'
                },
                {
                    name: '爬虫',
                    link: '/reptile',
                    icon: 'GitlabOutlined'
                }

            ],
            defaultSelectedKeys: [],
            collapsed: true
        };  
    }

    getIconsComponent(icon) {
        var IconsComponent = icons[icon];
        return (<IconsComponent />);
    }

    toggleCollapsed = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    activeItem = ({key}) => {
        this.defaultSelectedKeys = [key];
        let meun = this.state.meunList[key];
        location.href = meun.link;
    }

    render() {
        const { meunList } = this.state;
        return (
            <Layout.Sider>
                <Button type="primary" onClick={this.toggleCollapsed}>
                {React.createElement(this.state.collapsed ? icons.MenuUnfoldOutlined : icons.MenuFoldOutlined)}
                </Button>
                <Menu
                    mode="inline"
                    theme="dark"
                    defaultSelectedKeys={['0']}
                    inlineCollapsed={ this.state.collapsed }
                    onClick={ this.activeItem }
                >
                    { 
                        meunList.map( (meunItem, index)  =>
                            <Menu.Item 
                                key={index} 
                                icon={this.getIconsComponent(meunItem.icon)} 
                                title={meunItem.name}
                            >{meunItem.name}</Menu.Item>
                        )
                    }
                </Menu>
            </Layout.Sider>
        );
    }
}