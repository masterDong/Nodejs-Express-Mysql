let result = require('../config/result');
let resultErr = require('../config/resultErr');
let db = require('../config/db');
let async = require('async');

exports.index = function (req, res) {
	var current = req.query.current ? req.query.current : 1;
	var pageSize = req.query.pageSize ? req.query.pageSize : 10;
	let startIndex = (current - 1)*pageSize;
	let endIndex = current*pageSize;
	let sql = `SELECT * FROM collect_rule LIMIT ${startIndex} , ${endIndex}`;
	let sql2 = 'SELECT COUNT(*) as total FROM collect_rule';

	async.series({
        one: function (done) {
			db.query(sql, function(err, rows) {
				if (err) {
					done(err, null);
				} else {
					done(null, rows);
				}
			});
        },
        two: function (done) {
            db.query(sql2, function(err, rows) {
				if (err) {
					done(err, null);
				} else {
					done(null, rows);
				}
			});
        }
    }, function (err, data) {
		if (err) {
			result.code = -1000;
		} else {
			result.data = {
				list: data.one,
				total: data.two[0].total
			}
		}
		result.msg = resultErr[result.code];
		res.json(result);
    })
};

