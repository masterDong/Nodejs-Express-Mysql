let result = require('../config/result');
let resultErr = require('../config/resultErr');
let db = require('../config/db');
let async = require('async');

exports.getList = function (req, res) {
	var current = req.query.current ? req.query.current : 1;
	var pageSize = req.query.pageSize ? req.query.pageSize : 10;
	let startIndex = (current - 1)*pageSize;
	let endIndex = current*pageSize;
	let sql = `SELECT * FROM metting ORDER BY startTime DESC LIMIT ${startIndex} , ${endIndex}`;

	async.series({
        one: function (done) {
			db.query(sql, function(err, rows) {
				if (err) {
					done(err, null);
				} else {
					done(null, rows);
				}
			});
        }
    }, function (err, data) {
		if (err) {
			result.code = -1000;
		} else {
			result.code = 1;
			result.data = {
				list: data.one
			}
		}
		result.msg = resultErr[result.code];
		res.json(result);
    })
};

exports.getDetail = function (req, res) {
	var id = req.query.id;
	let sql = `SELECT * FROM metting WHERE id= ${id}`;
	if (!id) {
		result.code = -1013;
		result.msg = resultErr[result.code];
		res.json(result);
		return false;
	}
	async.series({
        one: function (done) {
			db.query(sql, function(err, rows) {
				if (err) {
					done(err, null);
				} else {
					done(null, rows);
				}
			});
        }
    }, function (err, data) {
		if (err) {
			result.code = -1000;
		} else if (data.one.length) {
			result.code = 1;
			result.data = data.one[0]
		} else {
			result.code = -1014;
		}
		result.msg = resultErr[result.code];
		res.json(result);
    })
};

exports.getEnroll = function (req, res) {
	var current = req.query.current ? req.query.current : 1;
	var pageSize = req.query.pageSize ? req.query.pageSize : 10;
	var openid = req.query.openid;
	let startIndex = (current - 1)*pageSize;
	let endIndex = current*pageSize;
	let sql = `SELECT * FROM mettingEnroll WHERE id= ${id} DESC LIMIT ${startIndex} , ${endIndex}`;

	if (!openid) {
		result.code = -1015;
		result.msg = 'openid'+resultErr[result.code];
		res.json(result);
		return false;
	}

	async.series({
        one: function (done) {
			db.query(sql, function(err, rows) {
				if (err) {
					done(err, null);
				} else {
					done(null, rows);
				}
			});
        }
    }, function (err, data) {
		if (err) {
			result.code = -1000;
		} else {
			result.code = 1;
			result.data = {
				list: data.one
			}
		}
		result.msg = resultErr[result.code];
		res.json(result);
    })
};

exports.addEnroll = function (req, res) {
	var openid = req.body.openid;
	var mettingId = req.body.mettingId;

	if (!openid) {
		result.code = -1015;
		result.msg = 'openid'+resultErr[result.code];
		res.json(result);
		return false;
	}

	if (!mettingId) {
		result.code = -1015;
		result.msg = 'mettingId'+resultErr[result.code];
		res.json(result);
		return false;
	}

	// 查询是否已经报名
	let sql = `SELECT * FROM mettingEnroll WHERE openid= ${openid} AND mettingId= ${mettingId}`;
	db.query(sql, function(err, rows) {
		if (err) {
			result.code = -1000;
			result.msg = resultErr[result.code];
			res.json(result);
			return false;
		} else if (rows.length){
			result.code = 1;
			result.msg = resultErr[result.code];
			res.json(result);
			return false;
		} else {
			let addSql = `INSERT INTO mettingEnroll (openid, mettingId, isCheck) VALUES (${openid}, ${mettingId}, 0)`;
			db.query(addSql, function(err, rows) {
				if (err) {
					result.code = -1000;
					result.msg = resultErr[result.code];
					res.json(result);
					return false;
				} else {
					result.code = 1;
					result.msg = resultErr[result.code];
					res.json(result);
					return false;
				}
			});
		}
	});
};

exports.confirmEnroll = function (req, res) {
	var openid = req.body.openid;
	var mettingId = req.body.mettingId;

	if (!openid) {
		result.code = -1015;
		result.msg = 'openid'+resultErr[result.code];
		res.json(result);
		return false;
	}

	if (!mettingId) {
		result.code = -1015;
		result.msg = 'mettingId'+resultErr[result.code];
		res.json(result);
		return false;
	}

	// 查询是否已经报名
	let sql = `SELECT * FROM mettingEnroll WHERE openid= ${openid} AND mettingId= ${mettingId}`;
	db.query(sql, function(err, rows) {
		if (err) {
			result.code = -1000;
			result.msg = resultErr[result.code];
			res.json(result);
			return false;
		} else if (rows.length){
			let addSql = `UPDATE mettingEnroll SET isCheck=1 WHERE openid= ${openid} AND mettingId= ${mettingId}`;
			db.query(addSql, function(err, rows) {
				if (err) {
					result.code = -1000;
					result.msg = resultErr[result.code];
					res.json(result);
					return false;
				} else {
					result.code = 1;
					result.msg = resultErr[result.code];
					res.json(result);
					return false;
				}
			});
		} else {
			result.code = -1016;
			result.msg = resultErr[result.code];
			res.json(result);
			return false;
		}
	});
};