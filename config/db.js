/**
 * Created by chendm on 2020/07/26.
 */
// 创建连接池
var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'a123456',
    database: 'notebook'
});

function  query(sql, values, callback) {
    pool.getConnection(function (err, connection) {
        if(err) throw err;
        // 利用连接池
        connection.query(sql, values,function (err, results, fields) {
            // 每次查询都会 回调
            callback(err, results);
            // 只是释放链接，在缓冲池了，没有被销毁
            connection.release();
            if(err) throw error;

        });

    });
}

exports.query = query;

