var test = require('../lib/mysql_testdb');
var async = require('async');



exports.get = function (req, res) {
  async.series({
    one: function (done) {
      test.index("SELECT * FROM testing", function (list) {
        done(null, list);
      });
    },
    two: function (done) {
      test.index("SELECT * FROM testing", function (list) {
        done(null, list);
      });
    }
  }, function (error, result) {
    res.json(result);
  })
};



exports.post = function (req, res) {
  async.series({
    one: function (done) {
      test.index("SELECT userid FROM testing", function (list) {
        done(null, list);
      });
    },
    two: function (done) {
      test.index("SELECT * FROM testing", function (list) {
        done(null, list);
      });
    }
  }, function (error, result) {
    res.json(result.one);
  });

  //test.index("SELECT * FROM testing", function (list) {
  //  console.log(list);
  //});
};
